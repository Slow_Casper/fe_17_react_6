import { Routes, Route } from 'react-router-dom';
import HomePage from './pages/HomePage/HomePage';
import CartPage from './pages/CartPage/CartPage';
import WishPage from './pages/WishPage/WishPage';


const AllRoutes = () => {

    return(
        <Routes>
            <Route path="/" element={<HomePage />} />
            <Route path="/cart-list" element={<CartPage />} />
            <Route path="/wish-list" element={<WishPage />} />
        </Routes>
    )
}

export default AllRoutes