import { getFromLocalStorage, saveToLocalStorage } from "../../utils/localStorage/localStorage";
import { REMOVE_ITEM, SET_IS_MODAL_CLOSE, SET_IS_MODAL_OPEN, SET_IS_REMOVE_MODAL } from "./actions";

const initialState = {
    isModalOpen: false,
    isRemoveModalOpen: false,
    item: {},
};

const modalReducer = (state = initialState, action) => {
    switch (action?.type) {
        case SET_IS_MODAL_OPEN: {
            return { ...state, isModalOpen: true, item: action.payload }
        }

        case SET_IS_MODAL_CLOSE: {
            return { ...state, isModalOpen: false, isRemoveModalOpen: false, item: {} }
        }

        case SET_IS_REMOVE_MODAL: {
            return { ...state, isRemoveModalOpen: true, item: action.payload }
        }

        case REMOVE_ITEM: {
            const newState = {...state.item}
            const getCart = getFromLocalStorage("cartList")

            const filteredCart = getCart.filter((el) => el.id !== newState.id)
            saveToLocalStorage("cartList", filteredCart)
            return { ...state, isRemoveModalOpen: false, item: {} }
        }
        
        default:
            return state;
    }
};

export default modalReducer;
