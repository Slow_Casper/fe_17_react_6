import { SET_IS_MODAL_OPEN, SET_IS_MODAL_CLOSE, SET_IS_REMOVE_MODAL, REMOVE_ITEM } from "./actions";


export const setModalOpen = (item) => ({type: SET_IS_MODAL_OPEN, payload: item})

export const setModalClose = () => ({type: SET_IS_MODAL_CLOSE})

export const setRemoveModal = (item) => ({type: SET_IS_REMOVE_MODAL, payload: item})

export const removeItem = () => ({type: REMOVE_ITEM})