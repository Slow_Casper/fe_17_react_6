import { removeItem, setModalClose, setModalOpen, setRemoveModal } from "./actionCreators";
import modalReducer from "./reducer";

describe("Modal reducers tests", () => {
    // TEST #1

    test("Should Modal Reducer return default values", () => {
        expect(modalReducer()).toEqual({
            isModalOpen: false,
            isRemoveModalOpen: false,
            item: {},
        });
    });

    // TEST #2

    test("Should Modal Reducer open", () => {
        expect(modalReducer(undefined, setModalOpen({ id: 1 }))).toEqual({
            isModalOpen: true,
            isRemoveModalOpen: false,
            item: { id: 1 },
        });
    });

    // TEST #3

    test("Should Modal Reducer close", () => {
        expect(modalReducer(undefined, setModalClose())).toEqual({
            isModalOpen: false,
            isRemoveModalOpen: false,
            item: {},
        });
    });

    // TEST #4

    test("Should Modal Reducer open remove window", () => {
        expect(modalReducer(undefined, setRemoveModal({ id: 1 }))).toEqual({
            isModalOpen: false,
            isRemoveModalOpen: true,
            item: { id: 1 },
        });
    });
});
