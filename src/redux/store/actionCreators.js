import { SET_STORE } from "./actions";
import axios from "axios"

export const setStore = (items) => ({type: SET_STORE, payload: items})

export const fetchItems = () => async (dispatch, getState) => {
    try {
        const { data } = await axios.get("./store/store.json")
        dispatch(setStore(data))
    } catch (error) {
        console.log(error)
    }
}