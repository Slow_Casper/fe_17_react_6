import storeReducer from "./reducer";

describe("Store reducers tests", () => {
    // TEST #1

    test("Should Store Reducer return default values", () => {
        expect(storeReducer()).toEqual({ items: [] });
    });
});
