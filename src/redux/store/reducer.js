import { SET_STORE } from "./actions";

const initialState = {
    items: [],
}

const storeReducer = (state = initialState, action) => {
    switch (action?.type) {
        case SET_STORE: {
            return { ...state, items: action.payload }
        }
    
        default: return state
    }
}

export default storeReducer