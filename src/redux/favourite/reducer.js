import { saveToLocalStorage } from "../../utils/localStorage/localStorage";
import { TOGGLE_FAVOURITE, SET_FAVOURITE } from "./actions";

const initialState = {
    items: [],
};

const favouriteReducer = (state = initialState, action) => {
    switch (action?.type) {

        case TOGGLE_FAVOURITE: {
            const newState = [...state.items];
            const item = action.payload;
            const index = newState.findIndex((el) => el.id === item.id)
            if (index < 0) {
                item.isFavourite = true
                newState.push(item);
            } else {
                item.isFavourite = false
                newState.splice(index, 1)
            }
            saveToLocalStorage("wishList", newState)
            return { ...state, items: newState }
        }

        case SET_FAVOURITE: {
            return { ...state, items: action.payload }
        }

        default:
            return state;
    }
};

export default favouriteReducer