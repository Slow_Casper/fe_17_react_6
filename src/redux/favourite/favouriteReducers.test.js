import { setFavourite, toggleFavourite } from "./actionCreators";
import { favouriteReducer } from "./reducer";

const fakeInitialState = {
    items: [
        { id: 1, isFavourite: true },
        { id: 2, isFavourite: true },
        { id: 3, isFavourite: true },
    ],
};

const fakeItems = [{ id: 1 }, { id: 2 }, { id: 3 }];

describe("Favourite reducers tests", () => {
    // TEST #1

    test("Should Favourite Reducer return default values", () => {
        expect(favouriteReducer()).toEqual({ items: [] });
    });

    // TEST #2

    test("Should Favourite Reducer add item", () => {
        expect(
            favouriteReducer(
                undefined,
                toggleFavourite({ id: 1, isFavourite: false })
            )
        ).toEqual({ items: [{ id: 1, isFavourite: true }] });
    });

    // TEST #3

    test("Should Favourite Reducer remove item", () => {
        expect(
            favouriteReducer(
                fakeInitialState,
                toggleFavourite({ id: 1, isFavourite: true })
            )
        ).toEqual({
            items: [
                { id: 2, isFavourite: true },
                { id: 3, isFavourite: true },
            ],
        });
    });

    // TEST #4

    test("Should Favourite Reducer add items", () => {
        expect(favouriteReducer(undefined, setFavourite(fakeItems))).toEqual({
            items: [{ id: 1 }, { id: 2 }, { id: 3 }],
        });
    });
});
