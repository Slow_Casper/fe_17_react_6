import { TOGGLE_FAVOURITE, SET_FAVOURITE } from "./actions";
import { getFromLocalStorage } from "../../utils/localStorage/localStorage"


export const toggleFavourite = (item) => ({type: TOGGLE_FAVOURITE, payload: item})

export const setFavourite = (items) => ({type: SET_FAVOURITE, payload: items})

export const getFavourite = () => async (dispatch) => {
    const data = await getFromLocalStorage("wishList")
    dispatch(setFavourite(data))
}