import { addToCart, removeFromCart, setCart } from "./actionCreators";
import cartReducer from "./reducer";


const fakeInitialState = {
    items: [
        {id: 1},
        {id: 2}
    ]
}

describe("Cart reducers tests", () => {
    
    // TEST #1

    test("Should Cart Reducer return default values", () => {
        expect(cartReducer()).toEqual({ items: [] });
    });

    // TEST #2

    test("Should Cart Reducer add item", () => {
        expect(cartReducer(undefined, addToCart({ id: 1, count: 0 }))).toEqual({
            items: [{ id: 1, count: 1 }],
        });
    });

    // TEST #3

    test("Should Cart Reducer set items", () => {
        expect(
            cartReducer(
                undefined,
                setCart([
                    { name: "Yurii" }
                ])
            )
        ).toEqual({
            items: [
                { name: "Yurii" }
            ],
        });
    });

    // TEST #4

    test("Should Cart Reducer remove item", () => {
        expect(
            cartReducer(
                fakeInitialState,
                removeFromCart(1)
            )
        ).toEqual({
            items: [
                {id: 2}
            ],
        });
    });

});
