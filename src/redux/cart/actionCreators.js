import { ADD_TO_CART, SET_CART, REMOVE_FROM_CART } from "./actions";
import { getFromLocalStorage } from "../../utils/localStorage/localStorage"


export const addToCart = (item) => ({type: ADD_TO_CART, payload: item})

export const setCart = (items) => ({type: SET_CART, payload: items})

export const removeFromCart = (id) => ({type: REMOVE_FROM_CART, payload: id})

export const getCart = () => async (dispatch) => {
    const data = await getFromLocalStorage("cartList")
    dispatch(setCart(data))
}