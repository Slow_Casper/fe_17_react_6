import { saveToLocalStorage } from "../../utils/localStorage/localStorage";
import { ADD_TO_CART, REMOVE_FROM_CART, SET_CART } from "./actions";

export const initialState = {
    items: [],
};

const cartReducer = (state = initialState, action) => {
    switch (action?.type) {
        case ADD_TO_CART: {
            const newState = [...state.items];
            const item = action.payload;
            const index = newState.findIndex((el) => el.id === item.id);
            if (index < 0) {
                item.count++
                newState.push(item);
            } else {
                newState[index].count++
            }
            saveToLocalStorage("cartList", newState);
            return { ...state, items: newState };
        }
        
        case SET_CART: {
            return { ...state, items: action.payload }
        }

        case REMOVE_FROM_CART: {
            const id = action.payload
            const newState = state.items.filter((el) => el.id !== id)

            saveToLocalStorage("cartList", newState)
            return { ...state, items: newState }
        }
        
        default:
            return state;
    }
};

export default cartReducer;
