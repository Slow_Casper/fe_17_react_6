import { render } from "@testing-library/react"
import Button from "./Button.jsx"

describe("Snapshot test Button component", () => {
    test("Should Button component render", () => {
        const { asFragment } = render(<Button />)

        expect(asFragment()).toMatchSnapshot()
    })
})