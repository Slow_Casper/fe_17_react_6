import { useField } from 'formik';
import React from 'react'
import { PatternFormat } from "react-number-format";


export const NumberInput = (props) => {

    const [field, meta] = useField(props.name)

  return (
    <div>
        <PatternFormat {...field} {...props} format="+38 (###) ### - ## - ##" allowEmptyFormatting mask="#" />
        {meta.touched && meta.error && <span>{meta.error}</span>}
    </div>
  )
}
