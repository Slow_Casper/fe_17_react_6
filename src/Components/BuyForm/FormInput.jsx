import { useField } from 'formik'
import React from 'react'

export const FormInput = (props) => {

    const [field, meta] = useField(props.name)

  return (  
    <div>
        <input {...props} {...field} />
        {meta.touched && meta.error && <p>{meta.error}</p>}
    </div>
  )
}
