import React from "react";
import { Form, Formik } from "formik";
import { FormInput } from "./FormInput";
import { object, string, number } from "yup";
import { NumberInput } from "./NumberInput";
import { useDispatch, useSelector } from "react-redux";
import { saveToLocalStorage } from "../../utils/localStorage/localStorage";
import { getCart } from "../../redux/cart/actionCreators";

import styles from "./BuyForm.module.scss";

export const validationSchema = object({
    name: string().min(3, "Name should have at least 3 symbols").required("Name is required"),
    secondName: string().min(3, "Name should have at least 3 symbols").required("Second Name is required"),
    age: number().required("Age is required"),
    address: string().required("Please, enter delivery address is required"),
    number: string().required(),
})

export const initialValues = {
    name: "",
    secondName: "",
    age: "",
    address: "",
    number: "",
};

export const BuyForm = () => {

    const cart = useSelector(state => state.cart.items)
    const dispatch = useDispatch()


    const onSubmit = (values, { resetForm }) => {
        console.log(cart)
        console.log(values)
        saveToLocalStorage("cartList", [])
        dispatch(getCart())
    };

    return (
        <div className={styles.container}>
            <Formik
                initialValues={initialValues}
                onSubmit={onSubmit}
                validationSchema={validationSchema}
            >
                <Form className={styles.form}>
                    <FormInput
                        name="name"
                        id="name"
                        type="text"
                        placeholder="Your Name"
                    />
                    <FormInput
                        name="secondName"
                        id="secondName"
                        type="text"
                        placeholder="Your Second Name"
                    />
                    <FormInput
                        name="age"
                        id="age"
                        type="text"
                        placeholder="What your age"
                    />
                    <FormInput
                        name="address"
                        id="address"
                        type="text"
                        placeholder="Delivery address"
                    />
                    <NumberInput
                        name="number"
                        id="number"
                        type="text"
                        placeholder="Contact number"
                    />
                    <button type="submit">Quick buy</button>
                </Form>
            </Formik>
        </div>
    );
};
