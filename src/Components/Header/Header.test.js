import { render } from "@testing-library/react";
import Header from "./Header";
import { BrowserRouter } from "react-router-dom";
import createMockStore from "redux-mock-store";
import { Provider } from "react-redux";

const mockStore = createMockStore({});

describe("Snapshot test Header component", () => {
    test("Should Header component render", () => {
        const store = mockStore({
            favourite: { items: [] },
            cart: { items: [] },
        });

        const { asFragment } = render(
            <Provider store={store}>
                <BrowserRouter>
                    <Header />
                </BrowserRouter>
            </Provider>
        );

        expect(asFragment()).toMatchSnapshot();
    });
});
