import styles from "./Header.module.scss";

import React from "react";
import { NavLink } from "react-router-dom";

import { ReactComponent as Cart } from "../../assets/svg/cart.svg";
import { ReactComponent as Wish } from "../../assets/svg/wish.svg";
import { useSelector } from "react-redux";

const Header = () => {

    const favourite = useSelector(state => state.favourite.items)
    const cart = useSelector(state => state.cart.items)


    let counter = 0;
    cart.forEach(({ count }) => counter += count)

    return (
        <section className={styles.header}>
            <div className={styles.container}>
                <div className={styles.buttons}>
                    <NavLink className={({ isActive }) => `${styles.navLink} ${isActive && styles.navLinkActive}`} to="/" >
                        <h1>HOME</h1>
                    </NavLink>
                    <NavLink className={({ isActive }) => `${styles.navLink} ${isActive && styles.navLinkActive}`} to="/cart-list" >
                        <Cart />
                        <span>{cart ? counter : 0}</span>
                    </NavLink>
                    <NavLink className={({ isActive }) => `${styles.navLink} ${isActive && styles.navLinkActive}`} to="/wish-list" >
                        <Wish />
                        <span>{favourite.length}</span>
                    </NavLink>
                </div>
            </div>
        </section>
    );
};

export default Header;
