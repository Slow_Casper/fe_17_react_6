import { useLocation } from "react-router-dom";
import { mapArr } from "../../utils/mapFunc/mapFunc";
import styles from "./StoreList.module.scss";
import { useSelector } from "react-redux";
import { memo } from "react";



const StoreList = () => {

    const { store, favourite, cart } = useSelector(state => state)
    const location = useLocation()

    return (
        <section className={styles.store}>
            <div className={styles.container}>
                {location.pathname === "/wish-list" ? mapArr(favourite.items) : location.pathname === "/cart-list" ? mapArr(cart.items) : mapArr(store.items)}
            </div>
        </section>
    )
}

export default memo(StoreList)