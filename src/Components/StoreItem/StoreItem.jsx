import styles from "./StoreItem.module.scss";
import Button from "../Button/Button";
import { ReactComponent as Star } from "../../assets/svg/star.svg";
import { ReactComponent as StarFilled } from "../../assets/svg/star_filled.svg";
import { useLocation } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { toggleFavourite } from "../../redux/favourite/actionCreators";
import { addToCart } from "../../redux/cart/actionCreators";
import { memo } from "react";
import { setModalOpen, setRemoveModal } from "../../redux/modal/actionCreators";

const StoreItem = ({ item }) => {
    const { name, price, url, width, height, article, color, id } = item;

    const dispatch = useDispatch();
    const location = useLocation();
    const isFavourite = useSelector(state => Boolean(state.favourite.items.find((el) => el.id === id)))

    const toggleFavor = () => {
        dispatch(toggleFavourite(item));
    };

    const funcAddToCart = () => {
        dispatch(addToCart(item))
        dispatch(setModalOpen(item))
    }

    const removeModal = () => {
        dispatch(setRemoveModal(item))
    }

    return (
        <>
            <div className={styles.card}>
                <img src={url} alt="#" width={width} height={height} />
                <p className={styles.article}>{article}</p>
                <h2 className={styles.title}>{name}</h2>
                <div className={styles.description}>
                    <div style={{ backgroundColor: color }}></div>
                    <p>{price}</p>
                </div>
                {location.pathname === "/cart-list" && (
                    <p className={styles.cartCount}>
                        В корзину додано {item.count} шт.
                    </p>
                )}
                <div className={styles.buttons}>
                    <Button
                        classes={styles.favBtn}
                        text={!isFavourite ? <Star /> : <StarFilled />}
                        onClick={toggleFavor}
                    />
                    {location.pathname === "/cart-list" ? (
                        <Button
                            classes={styles.btn}
                            text="Remove from cart"
                            onClick={removeModal}
                        />
                    ) : (
                        <Button
                            classes={styles.btn}
                            text="Add to cart"
                            onClick={funcAddToCart}
                        />
                    )}
                </div>
            </div>


        </>
    );
};

export default memo(StoreItem);
