import React from "react";
import StoreList from "../../Components/StoreList/StoreList";
import { BuyForm } from "../../Components/BuyForm/BuyForm";
import styles from "./CartPage.module.scss"

const CartPage = () => {
    return (
        <>
        <div className={styles.container}>
            <StoreList />
            <BuyForm />
        </div>
        </>
    );
};

export default CartPage;
