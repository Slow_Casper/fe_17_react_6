import React from "react";
import StoreList from "../../Components/StoreList/StoreList";
import styles from "./WishPage.module.scss"

const WishPage = () => {
    return (
        <div className={styles.container}>
            <StoreList />
        </div>
    );
};

export default WishPage;
