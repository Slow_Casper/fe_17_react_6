import React from "react";
import StoreList from "../../Components/StoreList/StoreList";
import styles from "./HomePage.module.scss"

const Home = () => {
    return (
        <>
            <div className={styles.container}>
                <StoreList />
            </div>
        </>
    );
};

export default Home;
